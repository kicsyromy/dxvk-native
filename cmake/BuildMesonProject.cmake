include (${CMAKE_CURRENT_LIST_DIR}/ExtractDependencies.cmake)

find_program (MESON_BIN meson DOC "Meson build system generator executable" REQUIRED)
find_program (NINJA_BIN ninja DOC "Ninja build system executable" REQUIRED)

function(build_meson_project)
    set (
        oneValueArgs
        PROJECT_NAME
        PROJECT_SOURCE_DIR
    )

    set (
        multiValueArgs
        DEPENDENCIES
        LINK_LIBRARIES
        PROJECT_OPTIONS
        ADDITIONAL_INCLUDE_PATHS
        ADDITIONAL_LIBRARY_PATHS
        ADDITIONAL_LIBRARIES
    )

    cmake_parse_arguments (
        MESON "" "${oneValueArgs}" "${multiValueArgs}" ${ARGN}
    )

    if (NOT MESON_PROJECT_NAME)
        message (FATAL_ERROR "Missing PROJECT_NAME")
    endif ()

    set (MESON_PROJECT_BUILD_DIR "${CMAKE_CURRENT_BINARY_DIR}/${MESON_PROJECT_NAME}")
    set (MESON_PREFIX "${MESON_PROJECT_BUILD_DIR}/staging")

    set (MESON_BINARY_DIR "bin")
    set (MESON_INCLUDE_DIR "include")
    set (MESON_LIBRARY_DIR "lib")
    set (MESON_BACKEND ninja)

    set (MESON_STRIP "--strip")
    string (TOLOWER ${CMAKE_BUILD_TYPE} MESON_BUILD_TYPE)
    if (${MESON_BUILD_TYPE} STREQUAL "debug")
        set (MESON_STRIP "")
    elseif (${MESON_BUILD_TYPE} STREQUAL "relwithdebuginfo")
        set (MESON_BUILD_TYPE debugoptimized)
        set (MESON_STRIP "")
    elseif (${MESON_BUILD_TYPE} STREQUAL "minsizerel")
        set (MESON_BUILD_TYPE minsize)
    else ()
        set (MESON_BUILD_TYPE plain)
    endif ()

    if (BUILD_SHARED_LIBS)
        set (MESON_DEFAULT_LIBRARY shared)
    else ()
        set (MESON_DEFAULT_LIBRARY static)
    endif ()

    add_library (
        ${MESON_PROJECT_NAME} INTERFACE
    )

    target_include_directories (
        ${MESON_PROJECT_NAME} INTERFACE SYSTEM
        ${MESON_PREFIX}/include
    )

    target_link_directories (
        ${MESON_PROJECT_NAME} INTERFACE
        ${MESON_PREFIX}/lib
    )

    foreach (DEP IN LISTS MESON_DEPENDENCIES)
        target_link_libraries (
            ${MESON_PROJECT_NAME} INTERFACE
            ${DEP}
        )
    endforeach ()

    extract_dependencies (
        TARGET ${MESON_PROJECT_NAME}
        INCLUDE_DIRECTORIES MESON_INC_DIRS
        LINK_DIRECTORIES MESON_LIB_DIRS
        LINK_LIBRARIES MESON_LIBS
    )

    foreach (LIB IN LISTS MESON_LINK_LIBRARIES)
        target_link_libraries (
            ${MESON_PROJECT_NAME} INTERFACE
            ${LIB}
        )
    endforeach ()

    foreach (INC_PATH IN LISTS MESON_ADDITIONAL_INCLUDE_PATHS)
        target_include_directories (
            ${MESON_PROJECT_NAME} SYSTEM INTERFACE
            ${INC_PATH}
        )
    endforeach ()

    foreach (LIB_PATH IN LISTS MESON_ADDITIONAL_LIBRARY_PATHS)
        target_link_directories (
            ${MESON_PROJECT_NAME} INTERFACE
            ${DEP}
        )
    endforeach ()

    foreach (LIB IN LISTS MESON_ADDITIONAL_LIBRARIES)
        target_link_libraries (
            ${MESON_PROJECT_NAME} INTERFACE
            ${LIB}
        )
    endforeach ()

    foreach (INC_DIR IN LISTS MESON_INC_DIRS)
        set (MESON_C_FLAGS "${MESON_C_FLAGS} -I${INC_DIR}")
        set (MESON_CXX_FLAGS "${MESON_C_FLAGS} -I${INC_DIR}")
    endforeach ()
    string (STRIP "${MESON_C_FLAGS}" MESON_C_FLAGS)
    string (STRIP "${MESON_C_FLAGS}" MESON_CXX_FLAGS)
    set (DEPENDENCY_CLFAGS "${MESON_C_FLAGS}")
    set (DEPENDENCY_CXXFLAGS@ "${MESON_CXX_FLAGS}")

    if (WIN32)
        list (JOIN MESON_LIB_DIRS "\;" MESON_LIBPATH)
        list (JOIN MESON_LIB_DIRS "\;" MESON_LIBRARY_PATH)
    else ()
        list (JOIN MESON_LIB_DIRS ":" MESON_LIBRARY_PATH)
    endif ()

    foreach (LIB_PATH IN LISTS MESON_LIB_DIRS)
        if (MSVC)
            set (MESON_LD_FLAGS "${MESON_LD_FLAGS} /LIBPATH:${LIB_PATH}")
        else ()
            set (MESON_LD_FLAGS "${MESON_LD_FLAGS} -L${LIB_PATH}")
        endif ()
    endforeach ()
    string (STRIP "${MESON_LD_FLAGS}" MESON_LD_FLAGS)

    foreach (LIB IN LISTS MESON_LIBS)
        if (MSVC)
            set (MESON_LD_FLAGS "${MESON_LD_FLAGS} ${LIB}.lib")
        else ()
            if (${LIB} MATCHES "^-.+")
                set (MESON_LD_FLAGS "${MESON_LD_FLAGS} ${LIB}")
            else ()
                set (MESON_LD_FLAGS "${MESON_LD_FLAGS} -l${LIB}")
            endif ()
        endif ()
    endforeach ()

    set (MESON_CONFIGURED_STAMP ${MESON_PROJECT_BUILD_DIR}/configure_stamp_${MESON_PROJECT_NAME})
    if (EXISTS ${MESON_CONFIGURED_STAMP})
        set (MESON_RECONFIGURE "--reconfigure")
    endif ()

    add_custom_target (
        ${MESON_PROJECT_NAME}-configure
        ${CMAKE_COMMAND} -E env
            CC=${CMAKE_C_COMPILER}
            CXX=${CMAKE_CXX_COMPILER}
            LIBRARY_PATH=${MESON_LIBRARY_PATH}
            LIBPATH=${MESON_LIBPATH}
            CFLAGS=${MESON_C_FLAGS}
            CXXFLAGS=${MESON_CXX_FLAGS}
            LDFLAGS=${MESON_LD_FLAGS}
            ${MESON_BIN}
                --prefix=${MESON_PREFIX}
                --bindir=${MESON_BINARY_DIR}
                --includedir=${MESON_INCLUDE_DIR}
                --libdir=${MESON_LIBRARY_DIR}
                --backend=${MESON_BACKEND}
                --buildtype=${MESON_BUILD_TYPE}
                --default-library=${MESON_DEFAULT_LIBRARY}
                ${MESON_STRIP}
                ${MESON_PROJECT_OPTIONS}
                ${MESON_RECONFIGURE}
                ${MESON_PROJECT_BUILD_DIR}
                ${MESON_PROJECT_SOURCE_DIR}
        VERBATIM
    )

    if (MESON_DEPENDENCIES)
        add_dependencies (
            ${MESON_PROJECT_NAME}-configure
            ${MESON_DEPENDENCIES}
        )
    endif ()

    add_custom_target (
        ${MESON_PROJECT_NAME}-configured-stamp
        ${CMAKE_COMMAND} -E touch ${MESON_CONFIGURED_STAMP}
    )

    add_dependencies (
        ${MESON_PROJECT_NAME}-configured-stamp
        ${MESON_PROJECT_NAME}-configure
    )

    add_custom_target (
        ${MESON_PROJECT_NAME}-build
        ${MESON_BIN} compile -C ${MESON_PROJECT_BUILD_DIR}
    )

    add_dependencies (
        ${MESON_PROJECT_NAME}-build
        ${MESON_PROJECT_NAME}-configured-stamp
    )

    add_custom_target (
        ${MESON_PROJECT_NAME}-stage
        ${MESON_BIN} install -C ${MESON_PROJECT_BUILD_DIR}
    )

    add_dependencies (
        ${MESON_PROJECT_NAME}-stage
        ${MESON_PROJECT_NAME}-build
    )

    add_dependencies (
        ${MESON_PROJECT_NAME}
        ${MESON_PROJECT_NAME}-stage
    )
endfunction()
