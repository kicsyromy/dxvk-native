include (${CMAKE_CURRENT_LIST_DIR}/Ament.cmake)

function (extract_dependencies)
    set (
        oneValueArgs
        TARGET
        INCLUDE_DIRECTORIES
        LINK_DIRECTORIES
        LINK_LIBRARIES
    )

    cmake_parse_arguments (
        ARGS "" "${oneValueArgs}" "" ${ARGN}
    )

    if (NOT ARGS_TARGET)
        message (FATAL_ERROR "Missing TARGET")
    endif ()

    if (NOT ARGS_INCLUDE_DIRECTORIES)
        message (FATAL_ERROR "Missing INCLUDE_DIRECTORIES")
    endif ()

    if (NOT ARGS_LINK_DIRECTORIES)
        message (FATAL_ERROR "Missing LINK_DIRECTORIES")
    endif ()

    if (NOT ARGS_LINK_LIBRARIES)
        message (FATAL_ERROR "Missing LINK_LIBRARIES")
    endif ()

    ament_get_recursive_properties (VAR_INCLUDE_DIRECTORIES VAR_LINK_LIBRARIES ${ARGS_TARGET})

    set (CLEAN_INCLUDE_DIRECTORIES)
    foreach (INCLUDE_DIR IN LISTS VAR_INCLUDE_DIRECTORIES)
        if (${INCLUDE_DIR} MATCHES "^\\$<INSTALL_INTERFACE:.+")
            continue ()
        endif ()
        string (REGEX REPLACE "^\\$<BUILD_INTERFACE:" "" INCLUDE_DIR ${INCLUDE_DIR})
        string (REGEX REPLACE ">$" "" INCLUDE_DIR ${INCLUDE_DIR})
        list (APPEND CLEAN_INCLUDE_DIRECTORIES ${INCLUDE_DIR})
    endforeach ()
    list (REMOVE_DUPLICATES CLEAN_INCLUDE_DIRECTORIES)

    set (CLEAN_LINK_DIRECTORIES)
    set (CLEAN_LINK_LIBRARIES)
    foreach (LIB IN LISTS VAR_LINK_LIBRARIES)
        if (${LIB} MATCHES "^-L.+")
            string (REGEX REPLACE "^-L" "" LIB ${LIB})
            list (APPEND CLEAN_LINK_DIRECTORIES ${LIB})
        else ()
            list (APPEND CLEAN_LINK_LIBRARIES ${LIB})
        endif ()
    endforeach ()
    list (REMOVE_DUPLICATES CLEAN_LINK_LIBRARIES)

    set(${ARGS_INCLUDE_DIRECTORIES} ${CLEAN_INCLUDE_DIRECTORIES} PARENT_SCOPE)
    set(${ARGS_LINK_DIRECTORIES} ${CLEAN_LINK_DIRECTORIES} PARENT_SCOPE)
    set(${ARGS_LINK_LIBRARIES} ${CLEAN_LINK_LIBRARIES} PARENT_SCOPE)
endfunction ()
