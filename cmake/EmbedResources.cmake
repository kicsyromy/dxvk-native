function (embed_resources)
    set (
        oneValueArgs
        TARGET
    )

    set (
        multiValueArgs
        RESOURCES
    )

    cmake_parse_arguments (
        CM "" "${oneValueArgs}" "${multiValueArgs}" ${ARGN}
    )
    
    if (PM_GTK_ARCH STREQUAL "xtensa")
        foreach (resource IN LISTS PM_GTK_RESOURCES)
            target_add_binary_data(${PM_GTK_TARGET} "${resource}" TEXT)
        endforeach ()
    endif ()
endfunction ()
