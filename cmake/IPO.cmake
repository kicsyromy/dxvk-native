option(PM_GTK_ENABLE_IPO "Enable Iterprocedural Optimization, aka Link Time Optimization (LTO)" ON)

if (PM_GTK_ENABLE_IPO)
    include(CheckIPOSupported)
    check_ipo_supported(RESULT PM_GTK_IPO_SUPPORTED OUTPUT output)
endif ()

function(enable_ipo project_name)
    if (PM_GTK_ENABLE_IPO)
        if (PM_GTK_IPO_SUPPORTED)
            message(STATUS "[PowerMonitorGtk] Enabling IPO for ${project_name}")
            set_property(
                TARGET ${project_name}
                PROPERTY INTERPROCEDURAL_OPTIMIZATION ON
            )
        else ()
            message(SEND_ERROR "[PowerMonitorGtk] IPO is not supported: ${output}")
        endif ()
    endif ()
endfunction()
