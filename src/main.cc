// http://www.directxtutorial.com/Lesson.aspx?lessonid=11-4-2

#include "application.hh"
#include "graphics_context.hh"
#include "window.hh"

#include <SDL2/SDL.h>

#ifdef WIN32
int WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
#else
int main()
#endif
{
    using namespace DX_NAMESPACE;

    try
    {
        const auto start = std::chrono::high_resolution_clock::now();

        auto app = Application{};

        auto window = DX_NAMESPACE::Window{ 640, 480, "My cool window" };
        window.set_render_callback([&start](GraphicsContext &ctx, RenderEvent &) noexcept {
            const auto time_since_start =
                static_cast<float>(std::chrono::duration_cast<std::chrono::milliseconds>(
                    std::chrono::high_resolution_clock::now() - start)
                                       .count()) /
                1000.F;
            const float c = std::sin(time_since_start) / 2.F + 0.5F;
            ctx.clear_buffer(c, c, 1.F);
        });

        return app.exec();
    } catch (const Exception &ex)
    {
        SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, ex.type().data(), ex.what(), nullptr);
    } catch (const std::exception &ex)
    {
        SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "Standard Exception", ex.what(), nullptr);
    } catch (...)
    {
        SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR,
            "Unknown Exception",
            "No details available",
            nullptr);
    }

    return EXIT_FAILURE;
}
