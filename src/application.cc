#include "application.hh"

#include "events/key_events.hh"
#include "events/mouse_events.hh"
#include "events/render_event.hh"
#include "events/window_events.hh"

#include <SDL2/SDL.h>

#include <unordered_set>
#include <variant>

namespace
{
    DX_NAMESPACE::Application *g_app_instance = nullptr;

    DX_NAMESPACE::Event *get_window_event(const SDL_WindowEvent &event_data,
        DX_NAMESPACE::WindowEventVariant &window_event_var)
    {
        using namespace DX_NAMESPACE;

        Event *window_event{ nullptr };
        switch ((event_data).event)
        {
        default:
            break;
        case SDL_WINDOWEVENT_SHOWN:
        case SDL_WINDOWEVENT_EXPOSED:
            window_event_var = DX_NAMESPACE::WindowShowEvent{};
            (window_event) = &(std::get<DX_NAMESPACE::WindowShowEvent>(window_event_var));
            break;
        case SDL_WINDOWEVENT_HIDDEN:
            window_event_var = DX_NAMESPACE::WindowHideEvent{};
            (window_event) = &(std::get<DX_NAMESPACE::WindowHideEvent>(window_event_var));
            break;
        case SDL_WINDOWEVENT_MOVED:
            window_event_var =
                DX_NAMESPACE::WindowMoveEvent{ (event_data).data1, (event_data).data2 };
            (window_event) = &(std::get<DX_NAMESPACE::WindowMoveEvent>(window_event_var));
            break;
        case SDL_WINDOWEVENT_RESIZED:
        case SDL_WINDOWEVENT_SIZE_CHANGED:
            window_event_var =
                DX_NAMESPACE::WindowResizeEvent{ static_cast<std::uint16_t>((event_data).data1),
                    static_cast<std::uint16_t>((event_data).data2) };
            (window_event) = &(std::get<DX_NAMESPACE::WindowResizeEvent>(window_event_var));
            break;
        case SDL_WINDOWEVENT_MINIMIZED:
        case SDL_WINDOWEVENT_MAXIMIZED:
        case SDL_WINDOWEVENT_RESTORED:
            break;
        case SDL_WINDOWEVENT_ENTER:
            window_event_var = DX_NAMESPACE::WindowMouseEnterEvent{};
            (window_event) = &(std::get<DX_NAMESPACE::WindowMouseEnterEvent>(window_event_var));
            break;
        case SDL_WINDOWEVENT_LEAVE:
            window_event_var = DX_NAMESPACE::WindowMouseLeaveEvent{};
            (window_event) = &(std::get<DX_NAMESPACE::WindowMouseLeaveEvent>(window_event_var));
            break;
        case SDL_WINDOWEVENT_FOCUS_GAINED:
            window_event_var = DX_NAMESPACE::WindowGainFocusEvent{};
            (window_event) = &(std::get<DX_NAMESPACE::WindowGainFocusEvent>(window_event_var));
            break;
        case SDL_WINDOWEVENT_FOCUS_LOST:
            window_event_var = DX_NAMESPACE::WindowLooseFocusEvent{};
            (window_event) = &(std::get<DX_NAMESPACE::WindowLooseFocusEvent>(window_event_var));
            break;
        case SDL_WINDOWEVENT_CLOSE:
            window_event_var = DX_NAMESPACE::WindowCloseEvent{};
            (window_event) = &(std::get<DX_NAMESPACE::WindowCloseEvent>(window_event_var));
            break;
        case SDL_WINDOWEVENT_TAKE_FOCUS:
        case SDL_WINDOWEVENT_HIT_TEST:
            break;
        }

        return window_event;
    }
} // namespace

DX_BEGIN_NAMESPACE

std::string_view Application::Exception::type() const noexcept
{
    return "Application Exception";
}

Application *Application::instance() noexcept
{
    return g_app_instance;
}

Application::Application()
{
    spdlog::set_level(spdlog::level::trace);

    if (g_app_instance != nullptr)
    {
        DX_LOG_EXCEPT(Exception, "Cannot create multiple instances of Application in one process");
    }

    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_JOYSTICK | SDL_INIT_EVENTS |
                 SDL_INIT_TIMER) < 0)
    {
        DX_LOG_EXCEPT(Exception, "Failed to initialize SDL: {}", SDL_GetError());
    }

    g_app_instance = this;
}

Application::~Application() noexcept
{
    g_app_instance = nullptr;
    SDL_Quit();
}

std::int32_t Application::exec()
{
    std::unordered_set<std::uint32_t> open_windows;

    auto start = std::chrono::high_resolution_clock::now();
    SDL_Event event;
    while (!quit_)
    {
        using namespace DX_NAMESPACE;

        while (SDL_PollEvent(&event) != 0)
        {
            if (event.type == SDL_QUIT)
            {
                quit_ = true;
            }
            else
            {
                switch (event.type)
                {
                case SDL_WINDOWEVENT: {
                    DX_NAMESPACE::WindowEventVariant window_event_var;
                    auto *window_event = get_window_event(event.window, window_event_var);

                    if (window_event != nullptr)
                    {
                        const auto window_id = event.window.windowID;
                        DX_LOG_DEBUG_F("New window event: {}", window_event->event_name());

                        auto &window_clients =
                            clients_[static_cast<std::size_t>(window_event->event_type())];
                        for (auto &client : window_clients)
                        {
                            if (client.callback() != nullptr &&
                                (client.id() == -1 ||
                                    (client.id() >= 0 && std::uint32_t(client.id()) == window_id)))
                            {
                                if (!client.callback()(client.id(),
                                        *window_event,
                                        client.callback_data()))
                                    break;
                            }
                        }

                        if (window_event->event_type() == EventType::WindowShown)
                        {
                            if (open_windows.empty()) {}

                            if (!open_windows.contains(window_id))
                            {
                                open_windows.insert(window_id);
                            }
                        }
                        else if (window_event->event_type() == EventType::WindowClosed)
                        {
                            open_windows.erase(window_id);
                            if (open_windows.empty())
                                quit_ = true;
                        }
                    }
                    break;
                }
                case SDL_MOUSEMOTION: {
                    MouseMoveEvent mouse_move_event{ event.motion.x, event.motion.y };
                    DX_LOG_DEBUG_F("Mouse moved: X: {}, Y: {}",
                        mouse_move_event.coordinates().first,
                        mouse_move_event.coordinates().second);

                    const auto window_id = event.motion.windowID;
                    if (SDL_CaptureMouse(mouse_button_pressed_ ? SDL_TRUE : SDL_FALSE) < 0)
                    {

                        DX_LOG_EXCEPT(Exception,
                            "Failed to {} mouse capture outside of the window: {}",
                            mouse_button_pressed_ ? "enable" : "disable",
                            SDL_GetError());
                    }

                    auto &mouse_event_clients =
                        clients_[static_cast<std::size_t>(MouseMoveEvent::event_type())];
                    for (auto &client : mouse_event_clients)
                    {
                        if (client.callback() != nullptr &&
                            (client.id() == -1 ||
                                (client.id() >= 0 && std::uint32_t(client.id()) == window_id)))
                        {
                            if (!client.callback()(client.id(),
                                    mouse_move_event,
                                    client.callback_data()))
                                break;
                        }
                    }
                    break;
                }
                case SDL_MOUSEBUTTONDOWN: {
                    const auto mouse_button = static_cast<MouseButton>(event.button.button - 1);
                    mouse_button_states_[static_cast<std::size_t>(mouse_button)] = 1;

                    const auto window_id = event.button.windowID;

                    MouseButtonPressEvent mouse_press_event{ event.button.x,
                        event.button.y,
                        mouse_button };
                    DX_LOG_DEBUG_F("Mouse button pressed: B: {}, X: {}, Y: {}",
                        static_cast<int>(mouse_press_event.button()),
                        mouse_press_event.coordinates().first,
                        mouse_press_event.coordinates().second);

                    auto &mouse_event_clients =
                        clients_[static_cast<std::size_t>(MouseButtonPressEvent::event_type())];
                    for (auto &client : mouse_event_clients)
                    {
                        if (client.callback() != nullptr &&
                            (client.id() == -1 ||
                                (client.id() >= 0 && std::uint32_t(client.id()) == window_id)))
                        {
                            if (!client.callback()(client.id(),
                                    mouse_press_event,
                                    client.callback_data()))
                                break;
                        }
                    }

                    mouse_button_pressed_ = true;

                    break;
                }
                case SDL_MOUSEBUTTONUP: {
                    const auto mouse_button = static_cast<MouseButton>(event.button.button - 1);
                    mouse_button_states_[static_cast<std::size_t>(mouse_button)] = 0;

                    const auto window_id = event.button.windowID;

                    MouseButtonReleaseEvent mouse_release_event{ event.button.x,
                        event.button.y,
                        mouse_button };
                    DX_LOG_DEBUG_F("Mouse button released: B: {}, X: {}, Y: {}",
                        static_cast<int>(mouse_release_event.button()),
                        mouse_release_event.coordinates().first,
                        mouse_release_event.coordinates().second);

                    auto &mouse_event_clients =
                        clients_[static_cast<std::size_t>(MouseButtonReleaseEvent::event_type())];
                    for (auto &client : mouse_event_clients)
                    {
                        if (client.callback() != nullptr &&
                            (client.id() == -1 ||
                                (client.id() >= 0 && std::uint32_t(client.id()) == window_id)))
                        {
                            if (!client.callback()(client.id(),
                                    mouse_release_event,
                                    client.callback_data()))
                                break;
                        }
                    }

                    mouse_button_pressed_ = false;

                    break;
                }
                case SDL_MOUSEWHEEL: {
                    break;
                }

                case SDL_KEYUP: {
                    const auto key = event.key.keysym.scancode;
                    key_states_[static_cast<std::size_t>(key)] = 0;

                    const auto window_id = event.key.windowID;

                    KeyReleaseEvent key_event{ KeyCode(key) };
                    DX_LOG_DEBUG_F("Key released: {}", key);

                    auto &key_event_clients =
                        clients_[static_cast<std::size_t>(KeyReleaseEvent::event_type())];
                    for (auto &client : key_event_clients)
                    {
                        if (client.callback() != nullptr &&
                            (client.id() == -1 ||
                                (client.id() >= 0 && std::uint32_t(client.id()) == window_id)))
                        {
                            if (!client.callback()(client.id(), key_event, client.callback_data()))
                                break;
                        }
                    }

                    key_pressed_ = false;

                    break;
                }
                case SDL_KEYDOWN: {
                    const auto key = event.key.keysym.scancode;
                    key_states_[static_cast<std::size_t>(key)] = 1;

                    const auto window_id = event.key.windowID;

                    KeyPressEvent key_event{ KeyCode(key) };
                    DX_LOG_DEBUG_F("Key pressed: {}", key);

                    auto &key_event_clients =
                        clients_[static_cast<std::size_t>(KeyPressEvent::event_type())];
                    for (auto &client : key_event_clients)
                    {
                        if (client.callback() != nullptr &&
                            (client.id() == -1 ||
                                (client.id() >= 0 && std::uint32_t(client.id()) == window_id)))
                        {
                            if (!client.callback()(client.id(), key_event, client.callback_data()))
                                break;
                        }
                    }

                    key_pressed_ = true;

                    break;
                }
                }
            }
        }

        const auto elapsed = float(std::chrono::duration_cast<std::chrono::milliseconds>(
                                 std::chrono::high_resolution_clock::now() - start)
                                       .count()) /
                             1000.F;
        start = std::chrono::high_resolution_clock::now();

        RenderEvent render_event{ elapsed, key_states_, mouse_button_states_ };
        auto &render_clients = clients_[static_cast<std::size_t>(EventType::Render)];
        for (auto &client : render_clients)
        {
            if (!client.callback()(-1, render_event, client.callback_data()))
                break;
        }
    }

    return EXIT_SUCCESS;
}

DX_END_NAMESPACE
