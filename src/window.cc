#include "window.hh"
#include "graphics_context.hh"
#include "utilities.hh"

#ifndef _WIN32
#include <native_sdl2.h>
#endif

#include <hedley/hedley.h>
#include <spdlog/fmt/fmt.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_syswm.h>

DX_BEGIN_NAMESPACE

std::string_view Window::Exception::type() const noexcept
{
    return "Window Exception";
}

Window::Window(std::uint16_t width, std::uint16_t height, std::string_view title)
  : width_{ width }
  , height_{ height }
  , window_handle_{ nullptr, &SDL_DestroyWindow }
{
    constexpr auto window_flags =
#ifdef _WIN32
        SDL_WINDOW_RESIZABLE;
#else
        SDL_WINDOW_RESIZABLE | SDL_WINDOW_VULKAN;
#endif

    if (title.ends_with('\0'))
    {
        window_handle_.reset(SDL_CreateWindow(title.data(),
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            width,
            height,
            window_flags));
    }
    else
    {
        const auto window_title = std::string{ title };
        window_handle_.reset(SDL_CreateWindow(window_title.c_str(),
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            width,
            height,
            window_flags));
    }

    if (!window_handle_)
    {
        DX_LOG_EXCEPT(Window::Exception, "Failed to create window: {}", SDL_GetError());
    }

#ifdef _WIN32
    SDL_SysWMinfo wmInfo;
    SDL_VERSION(&wmInfo.version);
    SDL_GetWindowWMInfo(window_handle_.get(), &wmInfo);
    hwnd_ = wmInfo.info.win.window;
#else
    hwnd_ = dxvk::wsi::toHwnd(window_handle_.get());
#endif

    HEDLEY_DIAGNOSTIC_PUSH
    DX_DIAGNOSTIC_IGNORE_USELESS_CAST
    gfx_context_ = std::make_unique<GraphicsContext>(static_cast<HWND>(hwnd_));
    HEDLEY_DIAGNOSTIC_POP

    auto *app = DX_APPLICATION();
    assert(app != nullptr);

    const auto window_id = static_cast<std::int32_t>(SDL_GetWindowID(window_handle_.get()));

    app->register_event_handler<KeyPressEvent, &Window::on_key_press_event>(this, window_id);

    app->register_event_handler<KeyReleaseEvent, &Window::on_key_release_event>(this, window_id);

    app->register_event_handler<MouseMoveEvent, &Window::on_mouse_move_event>(this, window_id);

    app->register_event_handler<MouseButtonPressEvent, &Window::on_mouse_button_press_event>(this,
        window_id);

    app->register_event_handler<MouseButtonReleaseEvent, &Window::on_mouse_button_release_event>(
        this,
        window_id);

    app->register_event_handler<WindowMouseEnterEvent, &Window::on_window_mouse_enter_event>(this,
        window_id);
    app->register_event_handler<WindowMouseLeaveEvent, &Window::on_window_mouse_leave_event>(this,
        window_id);

    app->register_event_handler<WindowResizeEvent, &Window::on_window_resized_event>(this,
        window_id);

    app->register_event_handler<WindowCloseEvent, &Window::on_window_closed_event>(this, window_id);

    app->register_event_handler<RenderEvent, &Window::on_render_event>(this, window_id);
}

Window::~Window() noexcept = default;

std::pair<std::size_t, std::size_t> Window::viewport_size() const noexcept
{
    return { width_, height_ };
}

GraphicsContext &Window::graphics_context() const noexcept
{
    return *gfx_context_;
}

void Window::set_title(std::string_view title) noexcept
{
    if (title.ends_with('\0'))
    {
        SDL_SetWindowTitle(window_handle_.get(), title.data());
    }
    else
    {
        const auto t = std::string{ title };
        SDL_SetWindowTitle(window_handle_.get(), t.c_str());
    }
}

bool Window::on_key_press_event(int window_id, KeyPressEvent &event) noexcept
{
    static_cast<void>(window_id);
    static_cast<void>(event);

    DX_LOG_DEBUG_F("[Window] key {} pressed", event.key_name());

    return true;
}

bool Window::on_key_release_event(int window_id, KeyReleaseEvent &event) noexcept
{
    static_cast<void>(window_id);
    static_cast<void>(event);

    DX_LOG_DEBUG_F("[Window] key {} released", event.key_name());

    return true;
}

bool Window::on_mouse_move_event(int window_id, MouseMoveEvent &event) noexcept
{
    static_cast<void>(window_id);
    static_cast<void>(event);

    DX_LOG_DEBUG("[Window] mouse move");

    return true;
}

bool Window::on_mouse_button_press_event(int window_id, MouseButtonPressEvent &event) noexcept
{
    static_cast<void>(window_id);

    DX_LOG_DEBUG_F("[Window] mouse button press B: {} X: {} Y: {}",
        event.button_name(),
        event.coordinates().first,
        event.coordinates().second);

    return true;
}

bool Window::on_mouse_button_release_event(int window_id, MouseButtonReleaseEvent &event) noexcept
{
    static_cast<void>(window_id);
    static_cast<void>(event);

    return true;
}

bool Window::on_window_mouse_enter_event(int window_id, WindowMouseEnterEvent &event) noexcept
{
    static_cast<void>(window_id);
    static_cast<void>(event);

    DX_LOG_DEBUG("[Window] mouse entered");

    return true;
}

bool Window::on_window_mouse_leave_event(int window_id, WindowMouseLeaveEvent &event) noexcept
{
    static_cast<void>(window_id);
    static_cast<void>(event);

    DX_LOG_DEBUG("[Window] mouse left");

    return true;
}

bool Window::on_window_resized_event(int window_id, WindowResizeEvent &event) noexcept
{
    static_cast<void>(window_id);
    static_cast<void>(event);

    return true;
}

bool Window::on_window_closed_event(int window_id, WindowCloseEvent &event) noexcept
{
    static_cast<void>(window_id);
    static_cast<void>(event);

    DX_LOG_DEBUG("[Window] window closed");

    hwnd_ = nullptr;
    window_handle_.reset();

    return true;
}

bool Window::on_render_event(int window_id, RenderEvent &event) noexcept
{
    static_cast<void>(window_id);

    if (render_callback_)
    {
        render_callback_(*gfx_context_, event);
    }

    gfx_context_->end_frame();

    return true;
}

DX_END_NAMESPACE
