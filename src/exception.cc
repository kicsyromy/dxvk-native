#include "exception.hh"

#include <spdlog/fmt/fmt.h>

DX_BEGIN_NAMESPACE

Exception::Exception(size_t line, const char *file, const char *message) noexcept
  : message_{ message }
  , line_{ line }
  , file_{ file }
{}

std::string_view Exception::type() const noexcept
{
    return "Exception";
}

const char *Exception::what() const noexcept
{
    const auto msg = message();

    if (msg.empty())
    {
        what_buffer_ = fmt::format("{}\n[File] {}\n[Line] {}\n", type(), file_, line_);
    }
    else
    {
        what_buffer_ =
            fmt::format("{}\n[Description] {}\n[File] {}\n[Line] {}\n", type(), msg, file_, line_);
    }

    return what_buffer_.c_str();
}

std::string_view Exception::message() const noexcept
{
    return message_;
}

HResultException::HResultException(size_t line, const char *file, HRESULT hr) noexcept
  : Exception{ line, file }
{
    auto error_description = hresult_description(hr);
    message_ = fmt::format("{}\n{}", error_description.first, error_description.second);
}

std::string_view HResultException::type() const noexcept
{
    return "HRESULT Exception";
}

DX_END_NAMESPACE
