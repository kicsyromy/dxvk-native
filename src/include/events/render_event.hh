#pragma once

#include "utilities.hh"

#include "events/event.hh"
#include "events/key_events.hh"
#include "events/mouse_events.hh"

#include <array>

DX_BEGIN_NAMESPACE

class RenderEvent : public EventBase<RenderEvent>
{
    DX_DECLARE_EVENT(Render, EventCategoryRender);

public:
    constexpr RenderEvent(float elapsed_seconds,
        const std::array<std::uint8_t, KEY_CODE_COUNT> &key_states,
        const std::array<std::uint8_t, MOUSE_BUTTON_COUNT> &mouse_button_states) noexcept
      : elapsed_seconds_{ elapsed_seconds }
      , key_states_{ key_states }
      , mouse_button_states_{ mouse_button_states }
    {}

public:
    constexpr float elapsed_seconds() const noexcept
    {
        return elapsed_seconds_;
    }

    constexpr bool key_pressed(KeyCode key) const noexcept
    {
        return key_states_[static_cast<std::size_t>(key)] != 0;
    }

    constexpr bool mouse_button_pressed(MouseButton button) const noexcept
    {
        return mouse_button_states_[static_cast<std::size_t>(button)] != 0;
    }

private:
    float elapsed_seconds_;
    const std::array<std::uint8_t, KEY_CODE_COUNT> &key_states_;
    const std::array<std::uint8_t, MOUSE_BUTTON_COUNT> &mouse_button_states_;
};

DX_END_NAMESPACE
