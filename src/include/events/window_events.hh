#pragma once

#include "events/event.hh"
#include "utilities.hh"

#include <variant>

DX_BEGIN_NAMESPACE

class WindowShowEvent : public EventBase<WindowShowEvent>
{
    DX_DECLARE_EVENT(WindowShown, EventCategoryWindow);

public:
    constexpr WindowShowEvent() noexcept = default;
};

class WindowHideEvent : public EventBase<WindowHideEvent>
{
    DX_DECLARE_EVENT(WindowHidden, EventCategoryWindow);

public:
    constexpr WindowHideEvent() noexcept = default;
};

class WindowCloseEvent : public EventBase<WindowCloseEvent>
{
    DX_DECLARE_EVENT(WindowClosed, EventCategoryWindow);

public:
    constexpr WindowCloseEvent() noexcept = default;
};

class WindowResizeEvent : public EventBase<WindowResizeEvent>
{
    DX_DECLARE_EVENT(WindowResized, EventCategoryWindow);

public:
    constexpr WindowResizeEvent(std::uint16_t width, std::uint16_t height) noexcept
      : EventBase<WindowResizeEvent>{}
      , width_{ width }
      , height_{ height }
    {}

public:
    constexpr std::pair<std::uint16_t, std::uint16_t> size() const noexcept
    {
        return { width_, height_ };
    }

private:
    std::uint16_t width_;
    std::uint16_t height_;
};

class WindowMouseEnterEvent : public EventBase<WindowMouseEnterEvent>
{
    DX_DECLARE_EVENT(WindowMouseEntered, EventCategoryWindow);

public:
    constexpr WindowMouseEnterEvent() noexcept = default;
};

class WindowMouseLeaveEvent : public EventBase<WindowMouseLeaveEvent>
{
    DX_DECLARE_EVENT(WindowMouseLeft, EventCategoryWindow);

public:
    constexpr WindowMouseLeaveEvent() noexcept = default;
};

class WindowGainFocusEvent : public EventBase<WindowGainFocusEvent>
{
    DX_DECLARE_EVENT(WindowGainedFocus, EventCategoryWindow);

public:
    constexpr WindowGainFocusEvent() noexcept = default;
};

class WindowLooseFocusEvent : public EventBase<WindowLooseFocusEvent>
{
    DX_DECLARE_EVENT(WindowLostFocus, EventCategoryWindow);

public:
    constexpr WindowLooseFocusEvent() noexcept = default;
};

class WindowMoveEvent : public EventBase<WindowMoveEvent>
{
    DX_DECLARE_EVENT(WindowMoved, EventCategoryWindow);

public:
    constexpr WindowMoveEvent(std::int32_t x, std::int32_t y) noexcept
      : EventBase<WindowMoveEvent>{}
      , x_{ x }
      , y_{ y }
    {}

public:
    constexpr std::pair<std::int32_t, std::int32_t> position() const noexcept
    {
        return { x_, y_ };
    }

private:
    std::int32_t x_;
    std::int32_t y_;
};

using WindowEventVariant = std::variant<std::monostate,
    WindowShowEvent,
    WindowHideEvent,
    WindowCloseEvent,
    WindowResizeEvent,
    WindowMouseEnterEvent,
    WindowMouseLeaveEvent,
    WindowGainFocusEvent,
    WindowLooseFocusEvent,
    WindowMoveEvent>;

DX_END_NAMESPACE
