#pragma once

#include <cstring>
#include <memory>

DX_BEGIN_NAMESPACE

#define DX_SINGLETON(klass) klass() = delete;

#define DX_DISABLE_COPY(klass)              \
    explicit klass(const klass &) = delete; \
    klass &operator=(const klass &) = delete

#define DX_DISABLE_MOVE(klass)         \
    explicit klass(klass &&) = delete; \
    klass &operator=(klass &&) = delete

#define DECL_RAII_HANDLE(type) using type##Handle = std::unique_ptr<type, void (*)(type *)>
#define CONSTEXPR_VALUE(exp) DX_NAMESPACE::utility::constexpr_value<decltype(exp), exp>::value
#define DECL_D3D_HANDLE(type, name)     \
    DECL_RAII_HANDLE(type);             \
    type##Handle name                   \
    {                                   \
        nullptr, [](type *i) noexcept { \
            i->Release();               \
        }                               \
    }

#define THROW_EXCEPTION(type, ...)                                                     \
    throw type                                                                         \
    {                                                                                  \
        __LINE__, ::DX_NAMESPACE::utility::get_file_name(__FILE__).data(), __VA_ARGS__ \
    }

#ifdef __clang__
#define DX_DIAGNOSTIC_IGNORE_OLD_STYLE_CAST \
    HEDLEY_PRAGMA(clang diagnostic ignored "-Wold-style-cast")
#define DX_DIAGNOSTIC_IGNORE_USELESS_CAST HEDLEY_PRAGMA(clang diagnostic ignored "-Wuseless-cast")
#elif __GNUG__
#define DX_DIAGNOSTIC_IGNORE_OLD_STYLE_CAST HEDLEY_PRAGMA(GCC diagnostic ignored "-Wold-style-cast")
#define DX_DIAGNOSTIC_IGNORE_USELESS_CAST HEDLEY_PRAGMA(GCC diagnostic ignored "-Wuseless-cast")
#else
#define DX_DIAGNOSTIC_IGNORE_OLD_STYLE_CAST
#define DX_DIAGNOSTIC_IGNORE_USELESS_CAST
#endif

namespace utility
{
    template<typename T, T v> struct constexpr_value
    {
        static constexpr const T value = v;
    };

    template<std::size_t S>
    constexpr std::size_t get_file_name_offset(const char (&path)[S], size_t i = S - 1) noexcept
    {
        return (path[i] == '/' || path[i] == '\\')
                   ? i + 1
                   : (i > 0 ? get_file_name_offset(path, i - 1) : 0);
    }

    template<std::size_t FilePathSize>
    static constexpr auto get_file_name(const char (&file_path)[FilePathSize]) noexcept
    {
        const auto file_name_offset = get_file_name_offset(file_path);
        std::array<char, FilePathSize + 1> result{};

        std::size_t position = 0;
        for (std::size_t i = 0; i < FilePathSize - file_name_offset - 1; ++i, ++position)
        {
            result[position] = file_path[i + file_name_offset];
        }
        result[position++] = '\0';

        return result;
    }

    template<typename...> struct GetClassType : std::false_type
    {
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...)>
    {
        using Type = Class;
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...) const>
    {
        using Type = Class;
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...) volatile>
    {
        using Type = Class;
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...) const volatile>
    {
        using Type = Class;
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...) const noexcept>
    {
        using Type = Class;
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...) noexcept>
    {
        using Type = Class;
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...) volatile noexcept>
    {
        using Type = Class;
    };
    template<typename R, typename Class, typename... Args>
    struct GetClassType<R (Class::*)(Args...) const volatile noexcept>
    {
        using Type = Class;
    };
} // namespace utility

DX_END_NAMESPACE
