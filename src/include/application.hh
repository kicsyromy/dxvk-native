#pragma once

#include "events/event.hh"
#include "events/key_events.hh"
#include "events/mouse_events.hh"
#include "exception.hh"
#include "logger.hh"
#include "utilities.hh"

#include <array>
#include <cstdint>
#include <memory>
#include <vector>

#define DX_APPLICATION() DX_NAMESPACE::Application::instance()

DX_BEGIN_NAMESPACE

class Application
{
public:
    class Exception : public ::DX_NAMESPACE::Exception
    {
    public:
        using ::DX_NAMESPACE::Exception::Exception;

    public:
        std::string_view type() const noexcept override;
    };

public:
    static Application *instance() noexcept;

public:
    using EventCallback = bool (*)(int, Event &, void *);
    template<typename Class, typename EvType>
    using EventCallbackMethod = bool (Class::*)(int, EvType &);

public:
    Application();
    ~Application() noexcept;

public:
    template<typename EvType,
        auto callback,
        typename Client = typename utility::GetClassType<decltype(callback)>::Type>
    void register_event_handler(Client *instance, int user_id = -1, int priority = 0) noexcept
    {
        static_assert(
            std::is_convertible_v<decltype(callback), EventCallbackMethod<Client, EvType>>);

        auto &event_clients = clients_[static_cast<std::size_t>(EvType::event_type())];

        event_clients.emplace_back(
            user_id,
            priority,
            [](int id, Event &e, void *c) -> bool {
                auto *client = static_cast<Client *>(c);
                auto &event = static_cast<EvType &>(e);
                return (client->*callback)(id, event);
            },
            instance);

        std::sort(event_clients.begin(),
            event_clients.end(),
            [](const EventClient &c1, const EventClient &c2) noexcept -> bool {
                return c1.priority() < c2.priority();
            });
    }

    void quit() noexcept
    {
        quit_ = true;
    }

    std::int32_t exec();

private:
    class EventClient
    {
    public:
        constexpr EventClient(int id,
            int priority,
            EventCallback event_cb,
            void *callback_data) noexcept
          : id_{ id }
          , priority_{ priority }
          , callback_{ event_cb }
          , callback_data_{ callback_data }
        {}

    public:
        [[nodiscard]] constexpr int id() const noexcept
        {
            return id_;
        }

        [[nodiscard]] constexpr int priority() const noexcept
        {
            return priority_;
        }

        [[nodiscard]] constexpr EventCallback callback() const noexcept
        {
            return callback_;
        }

        [[nodiscard]] constexpr void *callback_data() const noexcept
        {
            return callback_data_;
        }

    private:
        int id_;
        int priority_;
        EventCallback callback_;
        void *callback_data_;
    };

private:
    bool quit_{ false };
    std::array<std::uint8_t, KEY_CODE_COUNT> key_states_{ 0 };
    std::array<std::uint8_t, MOUSE_BUTTON_COUNT> mouse_button_states_{ 0 };
    std::array<std::vector<EventClient>, EVENT_TYPE_COUNT> clients_{};

    bool mouse_button_pressed_{ false };
    bool key_pressed_{ false };

private:
    DX_DISABLE_COPY(Application);
    DX_DISABLE_MOVE(Application);
};

DX_END_NAMESPACE
