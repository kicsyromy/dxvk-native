#pragma once

#include "hresult_descriptions.hh"
#include "utilities.hh"

#include <spdlog/spdlog.h>

#include <cstdio>

DX_BEGIN_NAMESPACE

#define DX_LOG_TO_STRING_IMP(x) #x
#define DX_LOG_TO_STRING(x) DX_LOG_TO_STRING_IMP(x)

#define DX_LOG_TRACE_F(format, ...)                                                           \
    do                                                                                        \
    {                                                                                         \
        constexpr auto format_specifier =                                                     \
            Logger::construct_format_specifier(__FILE__, DX_LOG_TO_STRING(__LINE__), format); \
        Logger::trace<format_specifier>(__VA_ARGS__);                                         \
    } while (false)
#define DX_LOG_TRACE(message) DX_LOG_TRACE_F("{}", message)

#define DX_LOG_DEBUG_F(format, ...)                                                           \
    do                                                                                        \
    {                                                                                         \
        constexpr auto format_specifier =                                                     \
            Logger::construct_format_specifier(__FILE__, DX_LOG_TO_STRING(__LINE__), format); \
        Logger::debug<format_specifier>(__VA_ARGS__);                                         \
    } while (false)
#define DX_LOG_DEBUG(message) DX_LOG_DEBUG_F("{}", message)

#define DX_LOG_INFO_F(format, ...)                                                            \
    do                                                                                        \
    {                                                                                         \
        constexpr auto format_specifier =                                                     \
            Logger::construct_format_specifier(__FILE__, DX_LOG_TO_STRING(__LINE__), format); \
        Logger::info<format_specifier>(__VA_ARGS__);                                          \
    } while (false)
#define DX_LOG_INFO(message) DX_LOG_INFO_F("{}", message)

#define DX_LOG_WARN_F(format, ...)                                                            \
    do                                                                                        \
    {                                                                                         \
        constexpr auto format_specifier =                                                     \
            Logger::construct_format_specifier(__FILE__, DX_LOG_TO_STRING(__LINE__), format); \
        Logger::warn<format_specifier>(__VA_ARGS__);                                          \
    } while (false)
#define DX_LOG_WARN(message) DX_LOG_WARN_F("{}", message)

#define DX_LOG_ERROR_F(format, ...)                                                           \
    do                                                                                        \
    {                                                                                         \
        constexpr auto format_specifier =                                                     \
            Logger::construct_format_specifier(__FILE__, DX_LOG_TO_STRING(__LINE__), format); \
        Logger::error<format_specifier>(__VA_ARGS__);                                         \
    } while (false)
#define DX_LOG_ERROR(message) DX_LOG_ERROR_F("{}", message)

#define DX_LOG_EXCEPT(type, ...)                                                     \
    do                                                                               \
    {                                                                                \
        const auto message = fmt::format(__VA_ARGS__);                               \
        constexpr auto file_name = ::DX_NAMESPACE::utility::get_file_name(__FILE__); \
        spdlog::error("{}:{}: {}", file_name.data(), __LINE__, message);             \
        throw type{ __LINE__, file_name.data(), message.c_str() };                   \
    } while (false)

#define DX_LOG_HR_EXCEPT(hresult)                                                      \
    do                                                                                 \
    {                                                                                  \
        auto error_description = hresult_description(hresult);                         \
        constexpr auto file_name = ::DX_NAMESPACE::utility::get_file_name(__FILE__);   \
        spdlog::error("{}:{}: {}: {}",                                                 \
            file_name.data(),                                                          \
            __LINE__,                                                                  \
            error_description.first,                                                   \
            error_description.second);                                                 \
        throw ::DX_NAMESPACE::HResultException{ __LINE__, file_name.data(), hresult }; \
    } while (false)

struct Logger
{
    template<auto format, typename... Args> static inline void trace(Args &&...args)
    {
        spdlog::trace(format.data(), std::forward<Args>(args)...);
    }

    template<auto format, typename... Args> static inline void debug(Args &&...args)
    {
        spdlog::debug(format.data(), std::forward<Args>(args)...);
    }

    template<auto format, typename... Args> static inline void info(Args &&...args)
    {
        spdlog::info(format.data(), std::forward<Args>(args)...);
    }

    template<auto format, typename... Args> static inline void warn(Args &&...args)
    {
        spdlog::warn(format.data(), std::forward<Args>(args)...);
    }

    template<auto format, typename... Args> static inline void error(Args &&...args)
    {
        spdlog::error(format.data(), std::forward<Args>(args)...);
    }

    template<std::size_t FilePathSize, std::size_t LineNumberSize, std::size_t FormatSpecifierSize>
    static constexpr auto construct_format_specifier(const char (&file_path)[FilePathSize],
        const char (&line)[LineNumberSize],
        const char (&format_specifier)[FormatSpecifierSize]) noexcept
    {
        const auto file_name_offset = utility::get_file_name_offset(file_path);
        std::array<char, FilePathSize + LineNumberSize + FormatSpecifierSize + 2 + 1> result{};

        std::size_t position = 0;
        for (std::size_t i = 0; i < FilePathSize - file_name_offset - 1; ++i, ++position)
        {
            result[position] = file_path[i + file_name_offset];
        }
        result[position++] = ':';
        for (std::size_t i = 0; i < LineNumberSize - 1; ++i, ++position)
        {
            result[position] = line[i];
        }
        result[position++] = ':';
        result[position++] = ' ';

        for (std::size_t i = 0; i < FormatSpecifierSize - 1; ++i, ++position)
        {
            result[position] = format_specifier[i];
        }

        result[position] = '\0';

        return result;
    }
};

DX_END_NAMESPACE
