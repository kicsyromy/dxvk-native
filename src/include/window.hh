#pragma once

#include "application.hh"
#include "exception.hh"
#include "utilities.hh"

#include "events/key_events.hh"
#include "events/mouse_events.hh"
#include "events/render_event.hh"
#include "events/window_events.hh"

#include <array>
#include <functional>
#include <memory>
#include <string_view>
#include <type_traits>
#include <utility>

using SDL_Window = struct SDL_Window;

DX_BEGIN_NAMESPACE

class GraphicsContext;

class Window
{
private:
    DECL_RAII_HANDLE(SDL_Window);

public:
    class Exception : public ::DX_NAMESPACE::Exception
    {
    public:
        using ::DX_NAMESPACE::Exception::Exception;

    public:
        std::string_view type() const noexcept override;
    };

public:
    Window(std::uint16_t width, std::uint16_t height, std::string_view title);
    ~Window() noexcept;

public:
    [[nodiscard]] std::pair<std::size_t, std::size_t> viewport_size() const noexcept;
    [[nodiscard]] GraphicsContext &graphics_context() const noexcept;

    void set_title(std::string_view title) noexcept;
    template<typename Callback> inline void set_render_callback(Callback &&cb) noexcept
    {
        render_callback_ = cb;
    }

private:
    bool on_key_press_event(int window_id, KeyPressEvent &event) noexcept;
    bool on_key_release_event(int window_id, KeyReleaseEvent &event) noexcept;
    bool on_mouse_move_event(int window_id, MouseMoveEvent &event) noexcept;
    bool on_mouse_button_press_event(int window_id, MouseButtonPressEvent &event) noexcept;
    bool on_mouse_button_release_event(int window_id, MouseButtonReleaseEvent &event) noexcept;
    bool on_window_mouse_enter_event(int window_id, WindowMouseEnterEvent &event) noexcept;
    bool on_window_mouse_leave_event(int window_id, WindowMouseLeaveEvent &event) noexcept;
    bool on_window_resized_event(int window_id, WindowResizeEvent &event) noexcept;
    bool on_window_closed_event(int window_id, WindowCloseEvent &event) noexcept;
    bool on_render_event(int window_id, RenderEvent &event) noexcept;

private:
    std::uint16_t width_;
    std::uint16_t height_;

private:
    std::function<void(GraphicsContext &gfx_context, RenderEvent &render_event)> render_callback_{};

private:
    SDL_WindowHandle window_handle_{ nullptr, nullptr };
    void *hwnd_{};

    std::unique_ptr<GraphicsContext> gfx_context_{ nullptr };
};

DX_END_NAMESPACE
