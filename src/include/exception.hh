#pragma once

#include "hresult_descriptions.hh"

#include <cstdint>
#include <exception>
#include <string>
#include <string_view>

DX_BEGIN_NAMESPACE

class Exception : public std::exception
{
public:
    Exception(std::size_t line, const char *file, const char *message = "") noexcept;

public:
    virtual std::string_view type() const noexcept;

    inline std::size_t line() const noexcept;
    inline std::string_view file() const noexcept;

public:
    const char *what() const noexcept override;

protected:
    virtual std::string_view message() const noexcept;

protected:
    std::string message_{};

private:
    std::size_t line_{};
    std::string file_{};

private:
    mutable std::string what_buffer_{};
};

inline size_t Exception::line() const noexcept
{
    return line_;
}

inline std::string_view Exception::file() const noexcept
{
    return file_;
}

class HResultException : public Exception
{
public:
    HResultException(std::size_t line, const char *file, HRESULT hr) noexcept;

public:
    std::string_view type() const noexcept override;
};

DX_END_NAMESPACE
