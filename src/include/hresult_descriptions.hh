#pragma once

#include <windows.h>

#include <magic_enum.hpp>

#include <string_view>
#include <unordered_map>

DX_BEGIN_NAMESPACE

namespace impl
{
    enum class D3DErrorCode
    {
        DX_S_OK,
        DX_S_FALSE,

        DX_E_INVALIDARG,
        DX_E_FAIL,
        DX_E_NOINTERFACE,
        DX_E_NOTIMPL,
        DX_E_OUTOFMEMORY,
        DX_E_POINTER,

        DX_DXGI_STATUS_OCCLUDED,
        DX_DXGI_STATUS_CLIPPED,
        DX_DXGI_STATUS_NO_REDIRECTION,
        DX_DXGI_STATUS_NO_DESKTOP_ACCESS,
        DX_DXGI_STATUS_GRAPHICS_VIDPN_SOURCE_IN_USE,
        DX_DXGI_STATUS_MODE_CHANGED,
        DX_DXGI_STATUS_MODE_CHANGE_IN_PROGRESS,
        DX_DXGI_ERROR_INVALID_CALL,
        DX_DXGI_ERROR_NOT_FOUND,
        DX_DXGI_ERROR_MORE_DATA,
        DX_DXGI_ERROR_UNSUPPORTED,
        DX_DXGI_ERROR_DEVICE_REMOVED,
        DX_DXGI_ERROR_DEVICE_HUNG,
        DX_DXGI_ERROR_DEVICE_RESET,
        DX_DXGI_ERROR_WAS_STILL_DRAWING,
        DX_DXGI_ERROR_FRAME_STATISTICS_DISJOINT,
        DX_DXGI_ERROR_GRAPHICS_VIDPN_SOURCE_IN_USE,
        DX_DXGI_ERROR_DRIVER_INTERNAL_ERROR,
        DX_DXGI_ERROR_NONEXCLUSIVE,
        DX_DXGI_ERROR_NOT_CURRENTLY_AVAILABLE,
        DX_DXGI_ERROR_REMOTE_CLIENT_DISCONNECTED,
        DX_DXGI_ERROR_REMOTE_OUTOFMEMORY,
    };

    inline const std::unordered_map<HRESULT, std::pair<std::string_view, std::string_view>>
        HRESULT_Description{
            { S_OK, { magic_enum::enum_name<D3DErrorCode::DX_S_OK>(), "Operation successful." } },
            { S_FALSE,
                { magic_enum::enum_name<D3DErrorCode::DX_S_FALSE>(), "Unspecified failure." } },

            { E_INVALIDARG,
                { magic_enum::enum_name<D3DErrorCode::DX_E_INVALIDARG>(),
                    "One or more arguments are not valid." } },
            { E_FAIL,
                { magic_enum::enum_name<D3DErrorCode::DX_E_FAIL>(), "Unspecified failure." } },
            { E_NOINTERFACE,
                { magic_enum::enum_name<D3DErrorCode::DX_E_NOINTERFACE>(),
                    "No such interface supported." } },
            { E_NOTIMPL,
                { magic_enum::enum_name<D3DErrorCode::DX_E_NOTIMPL>(), "Not implemented." } },
            { E_OUTOFMEMORY,
                { magic_enum::enum_name<D3DErrorCode::DX_E_OUTOFMEMORY>(),
                    "Failed to allocate necessary memory." } },
            { E_POINTER,
                { magic_enum::enum_name<D3DErrorCode::DX_E_POINTER>(),
                    "Pointer that is not valid." } },

            { DXGI_STATUS_OCCLUDED,
                { magic_enum::enum_name<D3DErrorCode::DX_DXGI_STATUS_OCCLUDED>(),
                    "The target window or output has been occluded. The application should suspend "
                    "rendering operations if possible." } },
            { DXGI_STATUS_CLIPPED,
                { magic_enum::enum_name<D3DErrorCode::DX_DXGI_STATUS_CLIPPED>(),
                    "Target window is clipped." } },
            { DXGI_STATUS_NO_REDIRECTION,
                { magic_enum::enum_name<D3DErrorCode::DX_DXGI_STATUS_NO_REDIRECTION>(), "" } },
            { DXGI_STATUS_NO_DESKTOP_ACCESS,
                { magic_enum::enum_name<D3DErrorCode::DX_DXGI_STATUS_NO_DESKTOP_ACCESS>(),
                    "No access to desktop." } },
            { DXGI_STATUS_GRAPHICS_VIDPN_SOURCE_IN_USE,
                { magic_enum::enum_name<
                      D3DErrorCode::DX_DXGI_STATUS_GRAPHICS_VIDPN_SOURCE_IN_USE>(),
                    "" } },
            { DXGI_STATUS_MODE_CHANGED,
                { magic_enum::enum_name<D3DErrorCode::DX_DXGI_STATUS_MODE_CHANGED>(),
                    "Display mode has changed" } },
            { DXGI_STATUS_MODE_CHANGE_IN_PROGRESS,
                { magic_enum::enum_name<D3DErrorCode::DX_DXGI_STATUS_MODE_CHANGE_IN_PROGRESS>(),
                    "Display mode is changing" } },
            { DXGI_ERROR_INVALID_CALL,
                { magic_enum::enum_name<D3DErrorCode::DX_DXGI_ERROR_INVALID_CALL>(),
                    "The application has made an erroneous API call that it had enough information "
                    "to avoid. This error is intended to denote that the application should be "
                    "altered to avoid the error. Use of the debug version of the DXGI.DLL will "
                    "provide run-time debug output with further information." } },
            { DXGI_ERROR_NOT_FOUND,
                { magic_enum::enum_name<D3DErrorCode::DX_DXGI_ERROR_NOT_FOUND>(),
                    "The item requested was not found. For GetPrivateData calls, this means that "
                    "the specified GUID had not been previously associated with the object." } },
            { DXGI_ERROR_MORE_DATA,
                { magic_enum::enum_name<D3DErrorCode::DX_DXGI_ERROR_MORE_DATA>(),
                    "The specified size of the destination buffer is too small to hold the "
                    "requested data." } },
            { DXGI_ERROR_UNSUPPORTED,
                { magic_enum::enum_name<D3DErrorCode::DX_DXGI_ERROR_UNSUPPORTED>(),
                    "Unsupported." } },
            { DXGI_ERROR_DEVICE_REMOVED,
                { magic_enum::enum_name<D3DErrorCode::DX_DXGI_ERROR_DEVICE_REMOVED>(),
                    "Hardware device removed." } },
            { DXGI_ERROR_DEVICE_HUNG,
                { magic_enum::enum_name<D3DErrorCode::DX_DXGI_ERROR_DEVICE_HUNG>(),
                    "Device hung due to badly formed commands." } },
            { DXGI_ERROR_DEVICE_RESET,
                { magic_enum::enum_name<D3DErrorCode::DX_DXGI_ERROR_DEVICE_RESET>(),
                    "Device reset due to a badly formed commant." } },
            { DXGI_ERROR_WAS_STILL_DRAWING,
                { magic_enum::enum_name<D3DErrorCode::DX_DXGI_ERROR_WAS_STILL_DRAWING>(),
                    "Was still drawing." } },
            { DXGI_ERROR_FRAME_STATISTICS_DISJOINT,
                { magic_enum::enum_name<D3DErrorCode::DX_DXGI_ERROR_FRAME_STATISTICS_DISJOINT>(),
                    "The requested functionality is not supported by the device or the driver." } },
            { DXGI_ERROR_GRAPHICS_VIDPN_SOURCE_IN_USE,
                { magic_enum::enum_name<D3DErrorCode::DX_DXGI_ERROR_GRAPHICS_VIDPN_SOURCE_IN_USE>(),
                    "The requested functionality is not supported by the device or the driver." } },
            { DXGI_ERROR_DRIVER_INTERNAL_ERROR,
                { magic_enum::enum_name<D3DErrorCode::DX_DXGI_ERROR_DRIVER_INTERNAL_ERROR>(),
                    "An internal driver error occurred." } },
            { DXGI_ERROR_NONEXCLUSIVE,
                { magic_enum::enum_name<D3DErrorCode::DX_DXGI_ERROR_NONEXCLUSIVE>(),
                    "The application attempted to perform an operation on an DXGI output that is "
                    "only legal after the output has been claimed for exclusive owenership." } },
            { DXGI_ERROR_NOT_CURRENTLY_AVAILABLE,
                { magic_enum::enum_name<D3DErrorCode::DX_DXGI_ERROR_NOT_CURRENTLY_AVAILABLE>(),
                    "The requested functionality is not supported by the device or the driver." } },
            { DXGI_ERROR_REMOTE_CLIENT_DISCONNECTED,
                { magic_enum::enum_name<D3DErrorCode::DX_DXGI_ERROR_REMOTE_CLIENT_DISCONNECTED>(),
                    "Remote desktop client disconnected." } },
            { DXGI_ERROR_REMOTE_OUTOFMEMORY,
                { magic_enum::enum_name<D3DErrorCode::DX_DXGI_ERROR_REMOTE_OUTOFMEMORY>(),
                    "Remote desktop client is out of memory." } },
        };
} // namespace impl

inline std::pair<std::string_view, std::string_view> hresult_description(HRESULT hr) noexcept
{
    using namespace impl;

    const auto it = HRESULT_Description.find(hr);
    if (it != HRESULT_Description.cend())
    {
        auto code = it->second.first;
        code.remove_prefix(3);

        return { code, it->second.second };
    }
    else
    {
        return { "UNKNOWN_ERROR", "An unknown error has occurred." };
    }
}

DX_END_NAMESPACE
