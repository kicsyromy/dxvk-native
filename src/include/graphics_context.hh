#pragma once

#include "exception.hh"
#include "utilities.hh"

#include <d3dx11.h>

#include <memory>
#include <string_view>

DX_BEGIN_NAMESPACE

class GraphicsContext
{
    class Exception : public ::DX_NAMESPACE::Exception
    {
    public:
        using ::DX_NAMESPACE::Exception::Exception;

    public:
        std::string_view type() const noexcept override;
    };

public:
    GraphicsContext(HWND window_handle);

public:
    void clear_buffer(float red, float green, float blue) noexcept;

    void end_frame();

private:
    DECL_D3D_HANDLE(ID3D11Device, device_);
    DECL_D3D_HANDLE(IDXGISwapChain, swap_chain_);
    DECL_D3D_HANDLE(ID3D11DeviceContext, device_context_);
    DECL_D3D_HANDLE(ID3D11RenderTargetView, render_target_view_);
};

DX_END_NAMESPACE
