#include "graphics_context.hh"

#include "logger.hh"

#include <spdlog/fmt/fmt.h>

#include <cstring>

DX_BEGIN_NAMESPACE

std::string_view GraphicsContext::Exception::type() const noexcept
{
    return "Graphics Context Exception";
}

GraphicsContext::GraphicsContext(HWND window_handle)
{
    /* Create a struct to hold information about the swap chain */
    DXGI_SWAP_CHAIN_DESC scd{};

    /* Fill the swap chain description struct */
    scd.BufferCount = 1;                                // one back buffer
    scd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; // use 32-bit color
    scd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;  // how swap chain is to be used
    scd.OutputWindow = window_handle;                   // the window to be used
    scd.SampleDesc.Count = 1;                           // how many multisamples
    scd.Windowed = TRUE;                                // windowed/full-screen mode

    /* Set up the pointers that we pass to the device and swap chain creation function */
    auto swap_chain = swap_chain_.get();
    auto device = device_.get();
    auto device_context = device_context_.get();

    /* Create a device, device context and swap chain using the information in the scd struct */
    auto result = D3D11CreateDeviceAndSwapChain(nullptr,
        D3D_DRIVER_TYPE_HARDWARE,
        nullptr,
        0,
        nullptr,
        0,
        D3D11_SDK_VERSION,
        &scd,
        &swap_chain,
        &device,
        nullptr,
        &device_context);
    if (FAILED(result))
    {
        DX_LOG_HR_EXCEPT(result);
    }

    auto render_target_view = render_target_view_.get();

    ID3D11Resource *back_buffer{ nullptr };
    result =
        swap_chain->GetBuffer(0, __uuidof(ID3D11Resource), reinterpret_cast<void **>(&back_buffer));
    if (FAILED(result))
    {
        DX_LOG_HR_EXCEPT(result);
    }
    result = device->CreateRenderTargetView(back_buffer, nullptr, &render_target_view);
    if (FAILED(result))
    {
        DX_LOG_HR_EXCEPT(result);
    }
    back_buffer->Release();

    /* Update the pointers we hold */
    swap_chain_.reset(swap_chain);
    device_.reset(device);
    device_context_.reset(device_context);
    render_target_view_.reset(render_target_view);
}

void GraphicsContext::clear_buffer(float red, float green, float blue) noexcept
{
    const float color[]{ red, green, blue, 1.F };
    device_context_->ClearRenderTargetView(render_target_view_.get(), color);
}

void GraphicsContext::end_frame()
{
    const auto result = swap_chain_->Present(1, 0);
    if (FAILED(result))
    {
        if (result == DXGI_ERROR_DEVICE_REMOVED)
        {
            DX_LOG_EXCEPT(Exception,
                "DXGI_ERROR_DEVICE_REMOVED\n{}",
                device_->GetDeviceRemovedReason());
        }
        else
        {
            DX_LOG_HR_EXCEPT(result);
        }
    }
}

DX_END_NAMESPACE
