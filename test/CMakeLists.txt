add_subdirectory (3rdparty)
include (Catch)

project (dx_tests)

add_executable (
    ${PROJECT_NAME}
    catch_main.cc
)

target_include_directories (
    ${PROJECT_NAME} PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}/include
    ${CMAKE_CURRENT_SOURCE_DIR}/../src
    ${CMAKE_CURRENT_SOURCE_DIR}/../src/include
)

target_compile_definitions (
    ${PROJECT_NAME} PRIVATE
    -DPM_GTK_TESTING
)

target_link_libraries (
    ${PROJECT_NAME} PRIVATE
    project_warnings
    project_options
    Catch2::Catch2
)

catch_discover_tests(
  ${PROJECT_NAME}
  TEST_PREFIX ""
  EXTRA_ARGS
      -s
      --reporter=xml
      --out=tests.xml
)
